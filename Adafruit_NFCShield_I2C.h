/**************************************************************************/
/*! 
    @file     Adafruit_NFCShield_I2C.h
    @author   Adafruit Industries
	@license  BSD (see license.txt)
	
	This is a library for the Adafruit PN532 NFC/RFID shield
	This library works with the Adafruit NFC breakout 
	----> https://www.adafruit.com/products/364
	
	Check out the links above for our tutorials and wiring diagrams 
	These chips use I2C to communicate
	
	Adafruit invests time and resources providing this open source code, 
	please support Adafruit and open-source hardware by purchasing 
	products from Adafruit!

	@section  HISTORY

    v1.3  - Modified to work with I2C
	
	v1.1  - Added full command list
          - Added 'verbose' mode flag to constructor to toggle debug output
          - Changed readPassiveTargetID() to return variable length values
	
*/
/**************************************************************************/

#ifndef Adafruit_NFCShield_I2C_h
#define Adafruit_NFCShield_I2C_h

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

#include <Wire.h>

#define PN532_PREAMBLE                      (0x00)
#define PN532_STARTCODE1                    (0x00)
#define PN532_STARTCODE2                    (0xFF)
#define PN532_POSTAMBLE                     (0x00)

#define PN532_HOSTTOPN532                   (0xD4)
#define PN532_PN532TOHOST                   (0xD5)

// PN532 Commands
#define PN532_COMMAND_DIAGNOSE              (0x00)
#define PN532_COMMAND_GETFIRMWAREVERSION    (0x02)
#define PN532_COMMAND_GETGENERALSTATUS      (0x04)
#define PN532_COMMAND_READREGISTER          (0x06)
#define PN532_COMMAND_WRITEREGISTER         (0x08)
#define PN532_COMMAND_READGPIO              (0x0C)
#define PN532_COMMAND_WRITEGPIO             (0x0E)
#define PN532_COMMAND_SETSERIALBAUDRATE     (0x10)
#define PN532_COMMAND_SETPARAMETERS         (0x12)
#define PN532_COMMAND_SAMCONFIGURATION      (0x14)
#define PN532_COMMAND_POWERDOWN             (0x16)
#define PN532_COMMAND_RFCONFIGURATION       (0x32)
#define PN532_COMMAND_RFREGULATIONTEST      (0x58)
#define PN532_COMMAND_INJUMPFORDEP          (0x56)
#define PN532_COMMAND_INJUMPFORPSL          (0x46)
#define PN532_COMMAND_INLISTPASSIVETARGET   (0x4A)
#define PN532_COMMAND_INATR                 (0x50)
#define PN532_COMMAND_INPSL                 (0x4E)
#define PN532_COMMAND_INDATAEXCHANGE        (0x40)
#define PN532_COMMAND_INCOMMUNICATETHRU     (0x42)
#define PN532_COMMAND_INDESELECT            (0x44)
#define PN532_COMMAND_INRELEASE             (0x52)
#define PN532_COMMAND_INSELECT              (0x54)
#define PN532_COMMAND_INAUTOPOLL            (0x60)
#define PN532_COMMAND_TGINITASTARGET        (0x8C)
#define PN532_COMMAND_TGSETGENERALBYTES     (0x92)
#define PN532_COMMAND_TGGETDATA             (0x86)
#define PN532_COMMAND_TGSETDATA             (0x8E)
#define PN532_COMMAND_TGSETMETADATA         (0x94)
#define PN532_COMMAND_TGGETINITIATORCOMMAND (0x88)
#define PN532_COMMAND_TGRESPONSETOINITIATOR (0x90)
#define PN532_COMMAND_TGGETTARGETSTATUS     (0x8A)

#define PN532_RESPONSE_INDATAEXCHANGE       (0x41)
#define PN532_RESPONSE_INLISTPASSIVETARGET  (0x4B)


#define PN532_WAKEUP                        (0x55)

#define PN532_SPI_STATREAD                  (0x02)
#define PN532_SPI_DATAWRITE                 (0x01)
#define PN532_SPI_DATAREAD                  (0x03)
#define PN532_SPI_READY                     (0x01)

#define PN532_I2C_ADDRESS                   (0x48 >> 1)
#define PN532_I2C_READBIT                   (0x01)
#define PN532_I2C_BUSY                      (0x00)
#define PN532_I2C_READY                     (0x01)
#define PN532_I2C_READYTIMEOUT              (20)

#define PN532_MIFARE_ISO14443A              (0x00)

// Mifare Commands
#define MIFARE_CMD_AUTH_A                   (0x60)
#define MIFARE_CMD_AUTH_B                   (0x61)
#define MIFARE_CMD_READ                     (0x30)
#define MIFARE_CMD_WRITE                    (0xA0)
#define MIFARE_CMD_TRANSFER                 (0xB0)
#define MIFARE_CMD_DECREMENT                (0xC0)
#define MIFARE_CMD_INCREMENT                (0xC1)
#define MIFARE_CMD_STORE                    (0xC2)

// DESFire Commands
#define DESFIRE_CMD_AUTH                    (0x0A)
#define DESFIRE_CMD_GET_KEYVERSION          (0x64)
#define DESFIRE_CMD_SELECT_APPLICATION      (0x5A)
#define DESFIRE_CMD_ADDITIONAL_FRAME        (0xAF)
#define DESFIRE_CMD_GET_VERSION             (0x60)
#define DESFIRE_CMD_GET_KEYSETTINGS         (0x45)
#define DESFIRE_CMD_CHANGE_KEY              (0xC4)
#define DESFIRE_CMD_CREATE_APPLICATION      (0xCA)
#define DESFIRE_CMD_CREATE_STD_FILE         (0xCD)
#define DESFIRE_CMD_CREATE_VAL_FILE         (0xCC)
#define DESFIRE_CMD_WRITE_STD_FILE          (0x3D)
#define DESFIRE_CMD_READ_STD_FILE           (0xBD)
#define DESFIRE_CMD_GETVALUE                (0x6C)
#define DESFIRE_CMD_CREDIT                  (0x0C)
#define DESFIRE_CMD_DEBIT                   (0xDC)
#define DESFIRE_CMD_FORMATPICC              (0xFC)
#define DESFIRE_CMD_COMMIT_TRANS            (0xC7)

// Prefixes for NDEF Records (to identify record type)
#define NDEF_URIPREFIX_NONE                 (0x00)
#define NDEF_URIPREFIX_HTTP_WWWDOT          (0x01)
#define NDEF_URIPREFIX_HTTPS_WWWDOT         (0x02)
#define NDEF_URIPREFIX_HTTP                 (0x03)
#define NDEF_URIPREFIX_HTTPS                (0x04)
#define NDEF_URIPREFIX_TEL                  (0x05)
#define NDEF_URIPREFIX_MAILTO               (0x06)
#define NDEF_URIPREFIX_FTP_ANONAT           (0x07)
#define NDEF_URIPREFIX_FTP_FTPDOT           (0x08)
#define NDEF_URIPREFIX_FTPS                 (0x09)
#define NDEF_URIPREFIX_SFTP                 (0x0A)
#define NDEF_URIPREFIX_SMB                  (0x0B)
#define NDEF_URIPREFIX_NFS                  (0x0C)
#define NDEF_URIPREFIX_FTP                  (0x0D)
#define NDEF_URIPREFIX_DAV                  (0x0E)
#define NDEF_URIPREFIX_NEWS                 (0x0F)
#define NDEF_URIPREFIX_TELNET               (0x10)
#define NDEF_URIPREFIX_IMAP                 (0x11)
#define NDEF_URIPREFIX_RTSP                 (0x12)
#define NDEF_URIPREFIX_URN                  (0x13)
#define NDEF_URIPREFIX_POP                  (0x14)
#define NDEF_URIPREFIX_SIP                  (0x15)
#define NDEF_URIPREFIX_SIPS                 (0x16)
#define NDEF_URIPREFIX_TFTP                 (0x17)
#define NDEF_URIPREFIX_BTSPP                (0x18)
#define NDEF_URIPREFIX_BTL2CAP              (0x19)
#define NDEF_URIPREFIX_BTGOEP               (0x1A)
#define NDEF_URIPREFIX_TCPOBEX              (0x1B)
#define NDEF_URIPREFIX_IRDAOBEX             (0x1C)
#define NDEF_URIPREFIX_FILE                 (0x1D)
#define NDEF_URIPREFIX_URN_EPC_ID           (0x1E)
#define NDEF_URIPREFIX_URN_EPC_TAG          (0x1F)
#define NDEF_URIPREFIX_URN_EPC_PAT          (0x20)
#define NDEF_URIPREFIX_URN_EPC_RAW          (0x21)
#define NDEF_URIPREFIX_URN_EPC              (0x22)
#define NDEF_URIPREFIX_URN_NFC              (0x23)

#define PN532_GPIO_VALIDATIONBIT            (0x80)
#define PN532_GPIO_P30                      (0)
#define PN532_GPIO_P31                      (1)
#define PN532_GPIO_P32                      (2)
#define PN532_GPIO_P33                      (3)
#define PN532_GPIO_P34                      (4)
#define PN532_GPIO_P35                      (5)

class Adafruit_NFCShield_I2C{
 public:
  Adafruit_NFCShield_I2C(uint8_t irq, uint8_t reset);
  void begin(void);
  
  // Generic PN532 functions
  boolean SAMConfig(void);
  uint32_t getFirmwareVersion(void);
  boolean sendCommandCheckAck(uint8_t *cmd, uint8_t cmdlen, uint16_t timeout = 1000);  
  boolean writeGPIO(uint8_t pinstate);
  uint8_t readGPIO(void);
  boolean setPassiveActivationRetries(uint8_t maxRetries);
  
  // ISO14443A functions
  boolean inListPassiveTarget();
  boolean readPassiveTargetID(uint8_t cardbaudrate, uint8_t * uid, uint8_t * uidLength);
//  boolean inDataExchange(uint8_t * send, uint8_t sendLength, uint8_t * response, uint8_t * responseLength);
  
  // Mifare DESFire functions

  uint8_t mifaredesfire_AuthStart (uint8_t * reply, uint8_t * replylength);
  uint8_t mifaredesfire_AuthStep2 (uint8_t * reply, uint8_t * replylength);
  uint8_t mifaredesfire_SelectApplication(uint8_t * app);
  uint8_t mifaredesfire_GetVersion(void);
  uint8_t mifaredesfire_GetKeySettings(uint8_t * reply, uint8_t * replylength);
 // uint8_t mifaredesfire_ChangeKey(uint8_t keynum, uint8_t* newkey, uint8_t newkeylen);
  uint8_t mifaredesfire_CreateApplication(uint8_t* appid, uint8_t keysettings, uint8_t numkeys);
  uint8_t mifaredesfire_CreateStandardFile(uint8_t filenum, uint8_t comm_settings, uint8_t* access, uint8_t* filesize);
  uint8_t mifaredesfire_CreateValueFile(uint8_t filenum, uint8_t comm_settings, uint8_t* access, uint8_t* lower_limit, uint8_t* upper_limit, uint8_t* value, uint8_t limited_credit);
  uint8_t mifaredesfire_WriteStandardFile(uint8_t filenum, uint8_t* offset, uint8_t length, uint8_t* data, uint8_t datalen);
  uint8_t mifaredesfire_AdditionalFrame(uint8_t* data, uint8_t datalen);
  uint8_t mifaredesfire_ReadStandardFile(uint8_t filenum, uint8_t* offset, uint8_t length, uint8_t* data, uint8_t datalen);
  uint8_t mifaredesfire_GetValue(uint8_t filenum, uint8_t* data);
  uint8_t mifaredesfire_Credit(uint8_t filenum, uint8_t* amount);
  uint8_t mifaredesfire_Debit(uint8_t filenum, uint8_t* amount);
  uint8_t mifaredesfire_FormatPICC(void);
  uint8_t mifaredesfire_CommitTransaction(void);


  // ISO 14443A CRC functions from libnfc
  void iso14443a_crc(uint8_t *pbtData, size_t szLen, uint8_t *pbtCrc);
  void iso14443a_crc_append(uint8_t *pbtData, size_t szLen);

  // Help functions to display formatted text
  static void PrintHex(const byte * data, const uint32_t numBytes);
  static void PrintHexChar(const byte * pbtData, const uint32_t numBytes);

 private:
  uint8_t _irq, _reset;
  uint8_t _uid[7];  // ISO14443A uid
  uint8_t _uidLen;  // uid len
  uint8_t _key[6];  // Mifare Classic key
  uint8_t inListedTag; // Tg number of inlisted tag.

  boolean readackframe(void);
  uint8_t wirereadstatus(void);
  void    wirereaddata(uint8_t* buff, uint8_t n);
  void    wiresendcommand(uint8_t* cmd, uint8_t cmdlen);
  boolean waitUntilReady(uint16_t timeout);
};

#endif
