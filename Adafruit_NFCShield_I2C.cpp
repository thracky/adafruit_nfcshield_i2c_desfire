/**************************************************************************/
/*! 
    @file     Adafruit_NFCShield_I2C.cpp
    @author   Adafruit Industries
	@license  BSD (see license.txt)
	
	I2C Driver for NXP's PN532 NFC/13.56MHz RFID Transceiver

	This is a library for the Adafruit PN532 NFC/RFID shields
	This library works with the Adafruit NFC breakout 
	----> https://www.adafruit.com/products/364
	
	Check out the links above for our tutorials and wiring diagrams 
	These chips use I2C to communicate
	
	Adafruit invests time and resources providing this open source code, 
	please support Adafruit and open-source hardware by purchasing 
	products from Adafruit!

	@section  HISTORY

    v1.4 - Added setPassiveActivationRetries()
	
    v1.3 - Modified to work with I2C
	
    v1.2 - Added writeGPIO()
         - Added readGPIO()

    v1.1 - Changed readPassiveTargetID() to handle multiple UID sizes
         - Added the following helper functions for text display
             static void PrintHex(const byte * data, const uint32_t numBytes)
             static void PrintHexChar(const byte * pbtData, const uint32_t numBytes)
         - Added the following Mifare Classic functions:
             bool mifareclassic_IsFirstBlock (uint32_t uiBlock)
             bool mifareclassic_IsTrailerBlock (uint32_t uiBlock)
             uint8_t mifareclassic_AuthenticateBlock (uint8_t * uid, uint8_t uidLen, uint32_t blockNumber, uint8_t keyNumber, uint8_t * keyData)
             uint8_t mifareclassic_ReadDataBlock (uint8_t blockNumber, uint8_t * data)
             uint8_t mifareclassic_WriteDataBlock (uint8_t blockNumber, uint8_t * data)
         - Added the following Mifare Ultalight functions:
             uint8_t mifareultralight_ReadPage (uint8_t page, uint8_t * buffer)	
*/
/**************************************************************************/
#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

#include <Wire.h>
#ifdef __AVR__
 #define WIRE Wire
#else // Arduino Due
 #define WIRE Wire1
#endif

#include "Adafruit_NFCShield_I2C.h"

byte pn532ack[] = {0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00};
byte pn532response_firmwarevers[] = {0x00, 0xFF, 0x06, 0xFA, 0xD5, 0x03};

// Uncomment these lines to enable debug output for PN532(I2C) and/or MIFARE related code
//#define PN532DEBUG
//#define MIFAREDEBUG

#define PN532_PACKBUFFSIZ 64
byte pn532_packetbuffer[PN532_PACKBUFFSIZ];

/**************************************************************************/
/*! 
    @brief  Sends a single byte via I2C

    @param  x    The byte to send
*/
/**************************************************************************/
static inline void wiresend(uint8_t x) 
{
  #if ARDUINO >= 100
    WIRE.write((uint8_t)x);
  #else
    WIRE.send(x);
  #endif
}

/**************************************************************************/
/*! 
    @brief  Reads a single byte via I2C
*/
/**************************************************************************/
static inline uint8_t wirerecv(void) 
{
  #if ARDUINO >= 100
    return WIRE.read();
  #else
    return WIRE.receive();
  #endif
}

/**************************************************************************/
/*! 
    @brief  Instantiates a new PN532 class

    @param  irq       Location of the IRQ pin
    @param  reset     Location of the RSTPD_N pin
*/
/**************************************************************************/
Adafruit_NFCShield_I2C::Adafruit_NFCShield_I2C(uint8_t irq, uint8_t reset) {
  _irq = irq;
  _reset = reset;

  pinMode(_irq, INPUT);
  pinMode(_reset, OUTPUT);
}

/**************************************************************************/
/*! 
    @brief  Setups the HW
*/
/**************************************************************************/
void Adafruit_NFCShield_I2C::begin() {
  WIRE.begin();

  // Reset the PN532  
  digitalWrite(_reset, HIGH);
  digitalWrite(_reset, LOW);
  delay(400);
  digitalWrite(_reset, HIGH);
}
 
/**************************************************************************/
/*! 
    @brief  Prints a hexadecimal value in plain characters

    @param  data      Pointer to the byte data
    @param  numBytes  Data length in bytes
*/
/**************************************************************************/
void Adafruit_NFCShield_I2C::PrintHex(const byte * data, const uint32_t numBytes)
{
  uint32_t szPos;
  for (szPos=0; szPos < numBytes; szPos++) 
  {
    Serial.print("0x");
    // Append leading 0 for small values
    if (data[szPos] <= 0xF)
      Serial.print("0");
    Serial.print(data[szPos]&0xff, HEX);
    if ((numBytes > 1) && (szPos != numBytes - 1))
    {
      Serial.print(" ");
    }
  }
  Serial.println("");
}

/**************************************************************************/
/*! 
    @brief  Prints a hexadecimal value in plain characters, along with
            the char equivalents in the following format

            00 00 00 00 00 00  ......

    @param  data      Pointer to the byte data
    @param  numBytes  Data length in bytes
*/
/**************************************************************************/
void Adafruit_NFCShield_I2C::PrintHexChar(const byte * data, const uint32_t numBytes)
{
  uint32_t szPos;
  for (szPos=0; szPos < numBytes; szPos++) 
  {
    // Append leading 0 for small values
    if (data[szPos] <= 0xF)
      Serial.print("0");
    Serial.print(data[szPos], HEX);
    if ((numBytes > 1) && (szPos != numBytes - 1))
    {
      Serial.print(" ");
    }
  }
  Serial.print("  ");
  for (szPos=0; szPos < numBytes; szPos++) 
  {
    if (data[szPos] <= 0x1F)
      Serial.print(".");
    else
      Serial.print((char)data[szPos]);
  }
  Serial.println("");
}
 
/**************************************************************************/
/*! 
    @brief  Checks the firmware version of the PN5xx chip

    @returns  The chip's firmware version and ID
*/
/**************************************************************************/
uint32_t Adafruit_NFCShield_I2C::getFirmwareVersion(void) {
  uint32_t response;

  pn532_packetbuffer[0] = PN532_COMMAND_GETFIRMWAREVERSION;

  if (! sendCommandCheckAck(pn532_packetbuffer, 1))
    return 0;
	
  // read data packet
  wirereaddata(pn532_packetbuffer, 12);
  
  // check some basic stuff
  if (0 != strncmp((char *)pn532_packetbuffer, (char *)pn532response_firmwarevers, 6)) {
    #ifdef PN532DEBUG
    Serial.println("Firmware doesn't match!");
	#endif
    return 0;
  }
  
  response = pn532_packetbuffer[7];
  response <<= 8;
  response |= pn532_packetbuffer[8];
  response <<= 8;
  response |= pn532_packetbuffer[9];
  response <<= 8;
  response |= pn532_packetbuffer[10];

  return response;
}


/**************************************************************************/
/*! 
    @brief  Sends a command and waits a specified period for the ACK

    @param  cmd       Pointer to the command buffer
    @param  cmdlen    The size of the command in bytes 
    @param  timeout   timeout before giving up
    
    @returns  1 if everything is OK, 0 if timeout occured before an
              ACK was recieved
*/
/**************************************************************************/
// default timeout of one second
boolean Adafruit_NFCShield_I2C::sendCommandCheckAck(uint8_t *cmd, uint8_t cmdlen, uint16_t timeout) {
  uint16_t timer = 0;

  // write the command
  wiresendcommand(cmd, cmdlen);
  
  // Wait for chip to say its ready!
  while (wirereadstatus() != PN532_I2C_READY) {
    if (timeout != 0) {
      timer+=10;
      if (timer > timeout)  
        return false;
    }
    delay(10);
  }
  
 #ifdef PN532DEBUG
  Serial.println("IRQ received");
 #endif
  
  // read acknowledgement
  if (!readackframe()) {
  #ifdef PN532DEBUG
    Serial.println("No ACK frame received!");
  #endif
    return false;
  }

  return true; // ack'd command
}

/**************************************************************************/
/*! 
    Writes an 8-bit value that sets the state of the PN532's GPIO pins
    
    @warning This function is provided exclusively for board testing and
             is dangerous since it will throw an error if any pin other
             than the ones marked "Can be used as GPIO" are modified!  All
             pins that can not be used as GPIO should ALWAYS be left high
             (value = 1) or the system will become unstable and a HW reset
             will be required to recover the PN532.
    
             pinState[0]  = P30     Can be used as GPIO
             pinState[1]  = P31     Can be used as GPIO
             pinState[2]  = P32     *** RESERVED (Must be 1!) ***
             pinState[3]  = P33     Can be used as GPIO
             pinState[4]  = P34     *** RESERVED (Must be 1!) ***
             pinState[5]  = P35     Can be used as GPIO
    
    @returns 1 if everything executed properly, 0 for an error
*/
/**************************************************************************/
boolean Adafruit_NFCShield_I2C::writeGPIO(uint8_t pinstate) {
  uint8_t errorbit;

  // Make sure pinstate does not try to toggle P32 or P34
  pinstate |= (1 << PN532_GPIO_P32) | (1 << PN532_GPIO_P34);
  
  // Fill command buffer
  pn532_packetbuffer[0] = PN532_COMMAND_WRITEGPIO;
  pn532_packetbuffer[1] = PN532_GPIO_VALIDATIONBIT | pinstate;  // P3 Pins
  pn532_packetbuffer[2] = 0x00;    // P7 GPIO Pins (not used ... taken by I2C)

/*   #ifdef PN532DEBUG
    Serial.print("Writing P3 GPIO: "); Serial.println(pn532_packetbuffer[1], HEX);
  #endif */

  // Send the WRITEGPIO command (0x0E)  
  if (! sendCommandCheckAck(pn532_packetbuffer, 3))
    return 0x0;
  
  // Read response packet (00 00 FF PLEN PLENCHECKSUM D5 CMD+1(0x0F) DATACHECKSUM)
  wirereaddata(pn532_packetbuffer, 8);

  #ifdef PN532DEBUG
    Serial.print("Received: ");
    PrintHex(pn532_packetbuffer, 8);
    Serial.println("");
  #endif  
  
  return  (pn532_packetbuffer[6] == 0x0F);
}

/**************************************************************************/
/*! 
    Reads the state of the PN532's GPIO pins
    
    @returns An 8-bit value containing the pin state where:
    
             pinState[0]  = P30     
             pinState[1]  = P31     
             pinState[2]  = P32     
             pinState[3]  = P33     
             pinState[4]  = P34     
             pinState[5]  = P35     
*/
/**************************************************************************/
uint8_t Adafruit_NFCShield_I2C::readGPIO(void) {
  pn532_packetbuffer[0] = PN532_COMMAND_READGPIO;

  // Send the READGPIO command (0x0C)  
  if (! sendCommandCheckAck(pn532_packetbuffer, 1))
    return 0x0;
  
  // Read response packet (00 00 FF PLEN PLENCHECKSUM D5 CMD+1(0x0D) P3 P7 IO1 DATACHECKSUM)
  wirereaddata(pn532_packetbuffer, 11);

  /* READGPIO response should be in the following format:
  
    byte            Description
    -------------   ------------------------------------------
    b0..6           Frame header and preamble
    b7              P3 GPIO Pins
    b8              P7 GPIO Pins (not used ... taken by I2C)
    b9              Interface Mode Pins (not used ... bus select pins) 
    b10             checksum */
  
/*  #ifdef PN532DEBUG
    Serial.print("Received: ");
    PrintHex(pn532_packetbuffer, 11);
    Serial.println("");
    Serial.print("P3 GPIO: 0x"); Serial.println(pn532_packetbuffer[7], HEX);
    Serial.print("P7 GPIO: 0x"); Serial.println(pn532_packetbuffer[8], HEX);
    Serial.print("IO GPIO: 0x"); Serial.println(pn532_packetbuffer[9], HEX);
    // Note: You can use the IO GPIO value to detect the serial bus being used
    switch(pn532_packetbuffer[9])
    {
      case 0x00:    // Using UART
        Serial.println("Using UART (IO = 0x00)");
        break;
      case 0x01:    // Using I2C 
        Serial.println("Using I2C (IO = 0x01)");
        break;
      case 0x02:    // Using I2C
        Serial.println("Using I2C (IO = 0x02)");
        break;
    }
  #endif
*/
  return pn532_packetbuffer[6];
}

/**************************************************************************/
/*! 
    @brief  Configures the SAM (Secure Access Module)
*/
/**************************************************************************/
boolean Adafruit_NFCShield_I2C::SAMConfig(void) {
  pn532_packetbuffer[0] = PN532_COMMAND_SAMCONFIGURATION;
  pn532_packetbuffer[1] = 0x01; // normal mode;
  pn532_packetbuffer[2] = 0x14; // timeout 50ms * 20 = 1 second
  pn532_packetbuffer[3] = 0x01; // use IRQ pin!
  
  if (! sendCommandCheckAck(pn532_packetbuffer, 4))
     return false;

  // read data packet
  wirereaddata(pn532_packetbuffer, 8);
  
  return  (pn532_packetbuffer[6] == 0x15);
}

/**************************************************************************/
/*! 
    Sets the MxRtyPassiveActivation byte of the RFConfiguration register
    
    @param  maxRetries    0xFF to wait forever, 0x00..0xFE to timeout
                          after mxRetries
    
    @returns 1 if everything executed properly, 0 for an error
*/
/**************************************************************************/
boolean Adafruit_NFCShield_I2C::setPassiveActivationRetries(uint8_t maxRetries) {
  pn532_packetbuffer[0] = PN532_COMMAND_RFCONFIGURATION;
  pn532_packetbuffer[1] = 5;    // Config item 5 (MaxRetries)
  pn532_packetbuffer[2] = 0xFF; // MxRtyATR (default = 0xFF)
  pn532_packetbuffer[3] = 0x01; // MxRtyPSL (default = 0x01)
  pn532_packetbuffer[4] = maxRetries;

#ifdef MIFAREDEBUG
  Serial.print("Setting MxRtyPassiveActivation to "); Serial.print(maxRetries, DEC); Serial.println(" ");
#endif
  
  if (! sendCommandCheckAck(pn532_packetbuffer, 5))
    return 0x0;  // no ACK
  
  return 1;
}

/***** ISO14443A Commands ******/

/**************************************************************************/
/*! 
    Waits for an ISO14443A target to enter the field
    
    @param  cardBaudRate  Baud rate of the card
    @param  uid           Pointer to the array that will be populated
                          with the card's UID (up to 7 bytes)
    @param  uidLength     Pointer to the variable that will hold the
                          length of the card's UID.
    
    @returns 1 if everything executed properly, 0 for an error
*/
/**************************************************************************/
boolean Adafruit_NFCShield_I2C::readPassiveTargetID(uint8_t cardbaudrate, uint8_t * uid, uint8_t * uidLength) {
  pn532_packetbuffer[0] = PN532_COMMAND_INLISTPASSIVETARGET;
  pn532_packetbuffer[1] = 1;  // max 1 cards at once (we can set this to 2 later)
  pn532_packetbuffer[2] = cardbaudrate;
  
  if (! sendCommandCheckAck(pn532_packetbuffer, 3))
  {
    #ifdef PN532DEBUG
	Serial.println("No card(s) read");
	#endif
    return 0x0;  // no cards read
  }
  
  // Wait for a card to enter the field
  uint8_t status = PN532_I2C_BUSY;
  #ifdef PN532DEBUG
  Serial.println("Waiting for IRQ (indicates card presence)");
  #endif
  while (wirereadstatus() != PN532_I2C_READY)
  {
	delay(10);
  }

  #ifdef PN532DEBUG
  Serial.println("Found a card"); 
  #endif
 
  // read data packet
  wirereaddata(pn532_packetbuffer, 20);
  
  // check some basic stuff
  /* ISO14443A card response should be in the following format:
  
    byte            Description
    -------------   ------------------------------------------
    b0..6           Frame header and preamble
    b7              Tags Found
    b8              Tag Number (only one used in this example)
    b9..10          SENS_RES
    b11             SEL_RES
    b12             NFCID Length
    b13..NFCIDLen   NFCID                                      */
  
#ifdef MIFAREDEBUG
    Serial.print("Found "); Serial.print(pn532_packetbuffer[7], DEC); Serial.println(" tags");
#endif
  if (pn532_packetbuffer[7] != 1) 
    return 0;
    
  uint16_t sens_res = pn532_packetbuffer[9];
  sens_res <<= 8;
  sens_res |= pn532_packetbuffer[10];
#ifdef MIFAREDEBUG
    Serial.print("ATQA: 0x");  Serial.println(sens_res, HEX); 
    Serial.print("SAK: 0x");  Serial.println(pn532_packetbuffer[11], HEX); 
#endif
  
  /* Card appears to be Mifare Classic */
  *uidLength = pn532_packetbuffer[12];
#ifdef MIFAREDEBUG
    Serial.print("UID:"); 
#endif
  for (uint8_t i=0; i < pn532_packetbuffer[12]; i++) 
  {
    uid[i] = pn532_packetbuffer[13+i];
#ifdef MIFAREDEBUG
      Serial.print(" 0x");Serial.print(uid[i], HEX); 
#endif
  }
#ifdef MIFAREDEBUG
    Serial.println();
#endif

  return 1;
}

uint8_t Adafruit_NFCShield_I2C::mifaredesfire_AuthStart (uint8_t * reply, uint8_t * replylength )
{   
    
  // Prepare the authentication command //
  pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;   /* Data Exchange Header */
  pn532_packetbuffer[1] = 0x01;                              /* Max card numbers */
  pn532_packetbuffer[2] = DESFIRE_CMD_AUTH;
  pn532_packetbuffer[3] = 0x00;                    /* Key Number 1-14 */

  if (! sendCommandCheckAck(pn532_packetbuffer, 4))
    return 0;

  // Read the response packet
  delay(100);
  wirereaddata(pn532_packetbuffer, 18);
  
  Serial.print("Reply from DESFire Auth: ");
  Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer, 18);
    
  // Check response for 0xAF at 9th byte
  if (pn532_packetbuffer[8] != 0xAF)
  {
    #ifdef PN532DEBUG
    Serial.print("Authentification failed: ");
    Adafruit_NFCShield_I2C::PrintHexChar(pn532_packetbuffer, 18);
    #endif
    return 0;
  }
  else {
    memcpy(reply, pn532_packetbuffer+9, 8);
    *replylength = 8;
  } 
  
  return 1;
}

uint8_t Adafruit_NFCShield_I2C::mifaredesfire_AuthStep2 (uint8_t * reply, uint8_t * replylength )
{
    pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
    pn532_packetbuffer[1] = 0x01;
    pn532_packetbuffer[2] = DESFIRE_CMD_ADDITIONAL_FRAME;
    memcpy(pn532_packetbuffer+3, reply, *replylength);

    if (! sendCommandCheckAck(pn532_packetbuffer, 19))
        return 0;
    delay(500);
    wirereaddata(pn532_packetbuffer, 18);

    Serial.print("Reply for step 2 of Auth: ");
    Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 9);

    if (pn532_packetbuffer[8] != 0x00)
    {
        #ifdef PN532DEBUG
            Serial.print("Reply from card not 0x00 for step 2 of auth");
            Adafruit_NFCShield_I2C::PrintHexChar(pn532_packetbuffer, 9);
        #endif    
        return 0;
    }
    else {
            memcpy(reply, pn532_packetbuffer+9, 8);
            *replylength = 8;
    }

    return 1;

}


uint8_t Adafruit_NFCShield_I2C::mifaredesfire_GetKeySettings(uint8_t * reply, uint8_t * replylength)
{
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_GET_KEYSETTINGS;

        if (! sendCommandCheckAck(pn532_packetbuffer, 3))
                return 0;
        wirereaddata(pn532_packetbuffer, 32);
        Serial.print("Reply from GKS");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer, 32);
}


uint8_t Adafruit_NFCShield_I2C::mifaredesfire_SelectApplication(uint8_t* app) {
  pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
  pn532_packetbuffer[1] = 0x01;
  pn532_packetbuffer[2] = DESFIRE_CMD_SELECT_APPLICATION;
  pn532_packetbuffer[3] = app[0];
  pn532_packetbuffer[4] = app[1];
  pn532_packetbuffer[5] = app[2];

  if (! sendCommandCheckAck(pn532_packetbuffer, 6))
      return 0;

  wirereaddata(pn532_packetbuffer, 16);

  Serial.print("Result of selecting application: ");
  Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer, 32);

return 1;

}

/**** BROKEN CHANGE KEY... only left here for reference
 * uint8_t Adafruit_NFCShield_I2C::mifaredesfire_ChangeKey(uint8_t keynum, uint8_t* enc_keydata, uint8_t keydatalen) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_CHANGE_KEY;
        pn532_packetbuffer[3] = 0x00;
        memcpy(pn532_packetbuffer+4, enc_keydata, keydatalen);
        
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer, 20);
        
        if(! sendCommandCheckAck(pn532_packetbuffer, 26, 5000)) {
                Serial.println("command failed... wtf");
                      wirereaddata(pn532_packetbuffer, 32);
                      Serial.print("Reply from key change");
                      Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer, 32);

                return 0;
        }
        delay(500);

        wirereaddata(pn532_packetbuffer, 32);
        Serial.print("Reply from key change");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer, 32);
        return 1;

}
*/

uint8_t Adafruit_NFCShield_I2C::mifaredesfire_CreateApplication(uint8_t* appid, uint8_t keysettings, uint8_t numkeys) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_CREATE_APPLICATION;
        memcpy(pn532_packetbuffer+3, appid, 3);
        pn532_packetbuffer[6] = keysettings;
        pn532_packetbuffer[7] = numkeys;

        if (! sendCommandCheckAck(pn532_packetbuffer, 8)) {
                return 0;
        }
        delay(500);
        wirereaddata(pn532_packetbuffer, 9);
        Serial.print("Reply from create application: ");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 1);
        if (pn532_packetbuffer[8] != 0x00) {
                Serial.print("Error creating application.");
                return 0;
        }
        return 1;
}

uint8_t Adafruit_NFCShield_I2C::mifaredesfire_CreateStandardFile(uint8_t filenum, uint8_t comm_settings, uint8_t* access, uint8_t* filesize) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_CREATE_STD_FILE;
        pn532_packetbuffer[3] = filenum;
        pn532_packetbuffer[4] = comm_settings;
        memcpy(pn532_packetbuffer+5, access, 2);
        memcpy(pn532_packetbuffer+7, filesize, 3);

        if (! sendCommandCheckAck(pn532_packetbuffer, 10)) {
                return 0;
        }
        delay(500);
        wirereaddata(pn532_packetbuffer, 9);
        Serial.print("Reply from create standard file: ");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 1);
        if (pn532_packetbuffer[8] != 0x00) {
               Serial.print("Error creating standard file.");
               return 0;
        }
        return 1;
}

uint8_t Adafruit_NFCShield_I2C::mifaredesfire_CreateValueFile(uint8_t filenum, uint8_t comm_settings, uint8_t* access, uint8_t* lower_limit, uint8_t* upper_limit, uint8_t* value, uint8_t limited_credit) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_CREATE_VAL_FILE;
        pn532_packetbuffer[3] = filenum;
        pn532_packetbuffer[4] = comm_settings;
        memcpy(pn532_packetbuffer+5, access, 2);
        memcpy(pn532_packetbuffer+7, lower_limit, 4);
        memcpy(pn532_packetbuffer+11, upper_limit, 4);
        memcpy(pn532_packetbuffer+15, value, 4);
        pn532_packetbuffer[19] = limited_credit;

        if (! sendCommandCheckAck(pn532_packetbuffer, 20)) {
                return 0;
        }
        delay(500);
        wirereaddata(pn532_packetbuffer, 9);
        Serial.print("Reply from create value file: ");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 1);
        if (pn532_packetbuffer[8] != 0x00) {
                Serial.print("Error creating standard file.");
                return 0;
        }
        return 1;
}


uint8_t Adafruit_NFCShield_I2C::mifaredesfire_WriteStandardFile(uint8_t filenum,  uint8_t* offset, uint8_t length, uint8_t* data, uint8_t datalen) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_WRITE_STD_FILE;
        pn532_packetbuffer[3] = filenum;
        memcpy(pn532_packetbuffer+4, offset, 3);
        pn532_packetbuffer[7] = length;
        pn532_packetbuffer[8] = 0x00;
        pn532_packetbuffer[9] = 0x00;
        memcpy(pn532_packetbuffer+10, data, datalen);

        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer, 10+datalen);

        if (! sendCommandCheckAck(pn532_packetbuffer, 10+datalen)) {
                return 0;
        }
        delay(500);
        wirereaddata(pn532_packetbuffer, 9);
        Serial.print("Reply from writing data to standard file: ");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 1);
        if (pn532_packetbuffer[8] != 0x00 && pn532_packetbuffer[8] != 0xAF) {
               Serial.print("Card did not reply with 0x00 or 0xAF.");
               return 0;
        }
        return 1;
 }

uint8_t Adafruit_NFCShield_I2C::mifaredesfire_AdditionalFrame(uint8_t* data, uint8_t datalen) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_ADDITIONAL_FRAME;
        memcpy(pn532_packetbuffer+3, data, datalen);

        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer, 3+datalen);
        if (! sendCommandCheckAck(pn532_packetbuffer, 3+datalen)) {
                return 0;                
        }
        delay(500);
        wirereaddata(pn532_packetbuffer, 9);
        Serial.print("Reply from sending AF:  ");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 1);
        if (pn532_packetbuffer[8] != 0xAF && pn532_packetbuffer[8] != 0x00) {
               Serial.print("Error writing to standard file.");
               return 0;
        }
        return 1;
}

uint8_t Adafruit_NFCShield_I2C::mifaredesfire_ReadStandardFile(uint8_t filenum,  uint8_t* offset, uint8_t length, uint8_t* data, uint8_t datalen) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_READ_STD_FILE;
        pn532_packetbuffer[3] = filenum;
        memcpy(pn532_packetbuffer+4, offset, 3);
        pn532_packetbuffer[7] = length;
        pn532_packetbuffer[8] = 0x00;
        pn532_packetbuffer[9] = 0x00;

        if (! sendCommandCheckAck(pn532_packetbuffer, 10)) {
                return 0;
        }
        delay(500);
        wirereaddata(pn532_packetbuffer, 32);
        Serial.print("Reply from read data from standard file: ");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 17);

        memcpy(data, pn532_packetbuffer+9, length+2);
        return 1;
}


uint8_t Adafruit_NFCShield_I2C::mifaredesfire_GetValue(uint8_t filenum,  uint8_t* data) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_GETVALUE;
        pn532_packetbuffer[3] = filenum;

        if (! sendCommandCheckAck(pn532_packetbuffer, 4)) {
                return 0;
        }
        delay(500);
        wirereaddata(pn532_packetbuffer, 32);
        Serial.print("Reply from get value: ");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 9);
        if(pn532_packetbuffer[8] != 0x00) {
                return 0;
        }
        memcpy(data, pn532_packetbuffer+9, 8);
        return 1;
}

uint8_t Adafruit_NFCShield_I2C::mifaredesfire_Credit(uint8_t filenum,  uint8_t* amount) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_CREDIT;
        pn532_packetbuffer[3] = filenum;
        memcpy(pn532_packetbuffer+4, amount, 8);

        if (! sendCommandCheckAck(pn532_packetbuffer, 12)) {
                return 0;
        }
        delay(500);
        wirereaddata(pn532_packetbuffer, 9);
        Serial.print("Reply from Credit: ");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 1);
        if(pn532_packetbuffer[8] != 0x00) {
                return 0;                       
        }
        return 1;
}

uint8_t Adafruit_NFCShield_I2C::mifaredesfire_Debit(uint8_t filenum,  uint8_t* amount) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_DEBIT;
        pn532_packetbuffer[3] = filenum;
        memcpy(pn532_packetbuffer+4, amount, 8);

        if (! sendCommandCheckAck(pn532_packetbuffer, 12)) {
                return 0;
        }
        delay(500);
        wirereaddata(pn532_packetbuffer, 9);
        Serial.print("Reply from Debit:  ");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 1);
        if(pn532_packetbuffer[8] != 0x00) {
                return 0;
        }
        return 1;
}

uint8_t Adafruit_NFCShield_I2C::mifaredesfire_FormatPICC(void) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_FORMATPICC;

        if (! sendCommandCheckAck(pn532_packetbuffer, 3)) {
                return 0;
        }
        delay(500);
        wirereaddata(pn532_packetbuffer, 32);
        Serial.print("Reply from Format: ");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 1);
}

uint8_t Adafruit_NFCShield_I2C::mifaredesfire_CommitTransaction(void) {
        pn532_packetbuffer[0] = PN532_COMMAND_INDATAEXCHANGE;
        pn532_packetbuffer[1] = 0x01;
        pn532_packetbuffer[2] = DESFIRE_CMD_COMMIT_TRANS;

        if (! sendCommandCheckAck(pn532_packetbuffer, 3)) {
                return 0;
        }
        delay(500);
        wirereaddata(pn532_packetbuffer, 32);
        Serial.print("Reply from Commit:  ");
        Adafruit_NFCShield_I2C::PrintHex(pn532_packetbuffer+8, 1);
        if(pn532_packetbuffer[8] != 0x00) {
                return 0;
        }
        return 1;
}



/* ISO 14443A CRC Functions from libnfc - iso14443-subr.c */
void Adafruit_NFCShield_I2C::iso14443a_crc(uint8_t *pbtData, size_t szLen, uint8_t *pbtCrc) {
        uint8_t  bt;
        uint32_t wCrc = 0x6363;
        do {
                bt = *pbtData++;
                bt = (bt ^ (uint8_t)(wCrc & 0x00FF));
                bt = (bt ^ (bt << 4));
                wCrc = (wCrc >> 8) ^ ((uint32_t) bt << 8) ^ ((uint32_t) bt << 3) ^ ((uint32_t) bt >> 4);
        } while (--szLen);
        *pbtCrc++ = (uint8_t)(wCrc & 0xFF);
        *pbtCrc = (uint8_t)((wCrc >> 8) & 0xFF);
}

void Adafruit_NFCShield_I2C::iso14443a_crc_append(uint8_t *pbtData, size_t szLen)
{
          iso14443a_crc(pbtData, szLen, pbtData + szLen);
}


/************** high level I2C */


/**************************************************************************/
/*! 
    @brief  Tries to read the PN532 ACK frame (not to be confused with 
	        the I2C ACK signal)
*/
/**************************************************************************/
boolean Adafruit_NFCShield_I2C::readackframe(void) {
  uint8_t ackbuff[6];
  
  wirereaddata(ackbuff, 6);
    
  return (0 == strncmp((char *)ackbuff, (char *)pn532ack, 6));
}

/************** mid level I2C */

/**************************************************************************/
/*! 
    @brief  Checks the IRQ pin to know if the PN532 is ready
	
	@returns 0 if the PN532 is busy, 1 if it is free
*/
/**************************************************************************/
uint8_t Adafruit_NFCShield_I2C::wirereadstatus(void) {
  uint8_t x = digitalRead(_irq);
  
  if (x == 1)
    return PN532_I2C_BUSY;
  else
    return PN532_I2C_READY;
}

/**************************************************************************/
/*! 
    @brief  Reads n bytes of data from the PN532 via I2C

    @param  buff      Pointer to the buffer where data will be written
    @param  n         Number of bytes to be read
*/
/**************************************************************************/
void Adafruit_NFCShield_I2C::wirereaddata(uint8_t* buff, uint8_t n) {
  uint16_t timer = 0;
  
  delay(2); 

#ifdef PN532DEBUG
  Serial.print("Reading: ");
#endif
  // Start read (n+1 to take into account leading 0x01 with I2C)
  WIRE.requestFrom((uint8_t)PN532_I2C_ADDRESS, (uint8_t)(n+2));
  // Discard the leading 0x01
  wirerecv();
  for (uint8_t i=0; i<n; i++) {
    delay(1);
    buff[i] = wirerecv();
#ifdef PN532DEBUG
    Serial.print(" 0x");
    Serial.print(buff[i], HEX);
#endif
  }
  // Discard trailing 0x00 0x00
  // wirerecv();
    
#ifdef PN532DEBUG
  Serial.println();
#endif
}

/**************************************************************************/
/*! 
    @brief  Writes a command to the PN532, automatically inserting the
            preamble and required frame details (checksum, len, etc.)

    @param  cmd       Pointer to the command buffer
    @param  cmdlen    Command length in bytes 
*/
/**************************************************************************/
void Adafruit_NFCShield_I2C::wiresendcommand(uint8_t* cmd, uint8_t cmdlen) {
  uint8_t checksum;

  cmdlen++;
  
#ifdef PN532DEBUG
  Serial.print("\nSending: ");
#endif

  delay(2);     // or whatever the delay is for waking up the board

  // I2C START
  WIRE.beginTransmission(PN532_I2C_ADDRESS);
  checksum = PN532_PREAMBLE + PN532_PREAMBLE + PN532_STARTCODE2;
  wiresend(PN532_PREAMBLE);
  wiresend(PN532_PREAMBLE);
  wiresend(PN532_STARTCODE2);

  wiresend(cmdlen);
  wiresend(~cmdlen + 1);
 
  wiresend(PN532_HOSTTOPN532);
  checksum += PN532_HOSTTOPN532;

#ifdef PN532DEBUG
  Serial.print(" 0x"); Serial.print(PN532_PREAMBLE, HEX);
  Serial.print(" 0x"); Serial.print(PN532_PREAMBLE, HEX);
  Serial.print(" 0x"); Serial.print(PN532_STARTCODE2, HEX);
  Serial.print(" 0x"); Serial.print(cmdlen, HEX);
  Serial.print(" 0x"); Serial.print(~cmdlen + 1, HEX);
  Serial.print(" 0x"); Serial.print(PN532_HOSTTOPN532, HEX);
#endif

   WIRE.write(cmd, cmdlen-1);
  for (uint8_t i=0; i<cmdlen-1; i++) {
   checksum += cmd[i];
#ifdef PN532DEBUG
   Serial.print(" 0x"); Serial.print(cmd[i], HEX);
#endif
  }
  
  wiresend(~checksum);
  wiresend(PN532_POSTAMBLE);
  
  // I2C STOP
  WIRE.endTransmission();

#ifdef PN532DEBUG
  Serial.print(" 0x"); Serial.print(~checksum, HEX);
  Serial.print(" 0x"); Serial.print(PN532_POSTAMBLE, HEX);
  Serial.println();
#endif
} 

/**************************************************************************/
/*! 
    @brief  Waits until the PN532 is ready.

    @param  timeout   Timeout before giving up
*/
/**************************************************************************/
boolean Adafruit_NFCShield_I2C::waitUntilReady(uint16_t timeout) {
  uint16_t timer = 0;
  while(wirereadstatus() != PN532_I2C_READY) {
    if (timeout != 0) {
      timer += 10;
      if (timer > timeout) {
        return false;
      }
    }
    delay(10);
  }
  return true;
}
 
/**************************************************************************/
/*! 
    @brief  'InLists' a passive target. PN532 acting as reader/initiator,
            peer acting as card/responder.
*/
/**************************************************************************/
boolean Adafruit_NFCShield_I2C::inListPassiveTarget() {
  pn532_packetbuffer[0] = PN532_COMMAND_INLISTPASSIVETARGET;
  pn532_packetbuffer[1] = 1;
  pn532_packetbuffer[2] = 0;
  
  #ifdef PN532DEBUG 
    Serial.print("About to inList passive target");
  #endif

  if (!sendCommandCheckAck(pn532_packetbuffer,3,1000)) {
    #ifdef PN532DEBUG
      Serial.println("Could not send inlist message");
    #endif
    return false;
  }

  if (!waitUntilReady(30000)) {
    return false;
  }

  wirereaddata(pn532_packetbuffer,sizeof(pn532_packetbuffer));
  
  if (pn532_packetbuffer[0] == 0 && pn532_packetbuffer[1] == 0 && pn532_packetbuffer[2] == 0xff) {
    uint8_t length = pn532_packetbuffer[3];
    if (pn532_packetbuffer[4]!=(uint8_t)(~length+1)) {
      #ifdef PN532DEBUG
        Serial.println("Length check invalid");
        Serial.println(length,HEX);
        Serial.println((~length)+1,HEX);
      #endif
      return false;
    }
    if (pn532_packetbuffer[5]==PN532_PN532TOHOST && pn532_packetbuffer[6]==PN532_RESPONSE_INLISTPASSIVETARGET) {
      if (pn532_packetbuffer[7] != 1) {
        #ifdef PN532DEBUG
        Serial.println("Unhandled number of targets inlisted");
        #endif
        Serial.println("Number of tags inlisted:");
        Serial.println(pn532_packetbuffer[7]);
        return false;
      }
      
      inListedTag = pn532_packetbuffer[8];
      Serial.print("Tag number: ");
      Serial.println(inListedTag);
      
      return true;
    } else {
      #ifdef PN532DEBUG
        Serial.print("Unexpected response to inlist passive host");
      #endif
      return false;
    } 
  } 
  else {
    #ifdef PN532DEBUG
      Serial.println("Preamble missing");
    #endif
    return false;
  }

  return true;
}
