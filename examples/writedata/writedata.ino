#include <Adafruit_NFCShield_I2C.h>
#include <DES.h>
#include <Wire.h>

#define IRQ   (2)
#define RESET (3)  // Not connected by default on the NFC Shield

Adafruit_NFCShield_I2C nfc(IRQ, RESET);
DES des;


void setup() {
  Serial.begin(115200);
  Serial.println("Hello!");

  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }
  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata >> 24) & 0xFF, HEX);
  Serial.print("Firmware ver. "); Serial.print((versiondata >> 16) & 0xFF, DEC);
  Serial.print('.'); Serial.println((versiondata >> 8) & 0xFF, DEC);

  // configure board to read RFID tags
  nfc.SAMConfig();

  Serial.println("Waiting for an ISO14443A Card ...");
}

void loop() {
  uint8_t success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

  //Arrays for outgoing commands/incoming replies
  uint8_t data[128];
  uint8_t datalen = 0;
  uint8_t application[3];

  byte key[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  byte desout[8];
  byte desin[8];
  uint8_t tempblock[8];
  uint8_t numblocks = 0;
  char text[] = "I’m sorry to have kept you waiting, but I’m afraid my walk has become rather sillier recently.";
  uint8_t textlen = (uint8_t)strlen(text);

  if (textlen % 8 == 0) {
    numblocks = textlen / 8;
  }
  else {
    numblocks = (textlen / 8) + 1;
  }
  int i;
  //copy text over to data array as uint8 so we can append crc.
  for (i = 0; i < textlen; i++) {
    data[i] = (uint8_t)text[i];
  }

  nfc.iso14443a_crc_append(data, textlen);

  //Text is now 2 bytes longer with CRC appended.
  textlen += 2;

  uint8_t std_filenum = 0x01;
  uint8_t offset[3];


  // Wait for an ISO14443A card
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);

  if (success) {
    Serial.println("Found an ISO14443A card");
    nfc.PrintHex(uid, uidLength);


    //If uidLength is 7 it's possible this is a DESFire card
    if (uidLength == 7) {

      uint8_t session_key[8];
      application[0] = 0xBE;
      application[1] = 0xEF;
      application[2] = 0x00;
      success = nfc.mifaredesfire_SelectApplication(application);

      if (success) {
        success = authenticate(key, session_key);

        if (success) {
          Serial.println("Authentication successful.");

          //Encipher all the things.
          for (i = 0; i < textlen+1; i++) {
            //If we just finished a block
            if (i % 8 == 0 && i > 7) {
              //If we didn't just finish the first block, we need to XOR with previous block before encryption
              if (i > 15) {
                xor_bytes(data+(i-16), tempblock, desin, 8);
                des.decrypt(desout, desin, session_key);
                memcpy(data + (i - 8), desout, 8);
              }
              else {
                memcpy(desin, tempblock, 8);
                des.decrypt(desout, desin, session_key);
                memcpy(data, desout, 8);
              }
            }

            //If there's no more text but we haven't filled the last block
            if (i == (textlen) && i % 8 != 0) {
              //Add padding until block is done, and encrypt the final block.
              tempblock[i % 8] = (byte)data[i];
              i += 1;
              while (i % 8 != 0) {
                tempblock[i % 8] = 0x00;
                i += 1;
              }
              xor_bytes(data + (i - 16), tempblock, desin, 8);
              des.decrypt(desout, desin, session_key);
              memcpy(data + (i - 8), desout, 8);
            }
            else {
              tempblock[i % 8] = (byte)data[i];
            }
          }
          
          nfc.PrintHex(data, numblocks*8);
          uint8_t sent_blocks;
          uint8_t blocks_queued = 0;

          offset[0] = 0x00;
          offset[1] = 0x00;
          offset[2] = 0x00;

          //Send first data block with the command.
          success = nfc.mifaredesfire_WriteStandardFile(std_filenum, offset, (textlen - 2), data, 8);

          if (success) {
            for (sent_blocks = 1; sent_blocks < numblocks; sent_blocks++) {
              //If we already have a block queued up.
              if (blocks_queued == 1) {
                //sent_blocks-1 because we need to include the previous block also.
                success = nfc.mifaredesfire_AdditionalFrame(data + ((sent_blocks - 1) * 8), 16);
                if (success) {
                  blocks_queued = 0;
                  continue;
                }
                else {
                  blocks_queued = 0;
                  break;
                }
              }
              //If we're here, it's the last block and it's by itself.
              if (sent_blocks == numblocks - 1) {
                success = nfc.mifaredesfire_AdditionalFrame(data + (sent_blocks * 8), 8);
                if (success) {
                  break;
                }
              }
              //Otherwise we just increment our "queue"
              else {
                blocks_queued++;
              }
            }
            if (sent_blocks == numblocks) {
              Serial.println("All blocks reported as sent");
            }
          }
        }
      }

    }
  }
}
