#include <Adafruit_NFCShield_I2C.h>
#include <DES.h>
#include <Wire.h>

#define IRQ   (2)
#define RESET (3)  // Not connected by default on the NFC Shield

Adafruit_NFCShield_I2C nfc(IRQ, RESET);
DES des;


void setup() {
  Serial.begin(115200);
  Serial.println("Hello!");

  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }
  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  
  // configure board to read RFID tags
  nfc.SAMConfig();
  
  Serial.println("Waiting for an ISO14443A Card ...");
}



void loop() {
  uint8_t success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
  
  //Arrays for outgoing commands/incoming replies
  uint8_t data[32];
  uint8_t datalen = 0;
  uint8_t application[3];
  
  uint8_t std_filenum = 0x01;
  uint8_t val_filenum = 0x02;
  
  uint8_t comm_settings = 0x03; //bit 0 and bit 1 active = fully encrypted communication.
  uint8_t access[] = { 0x00, 0x1F };
  uint8_t filesize[] = { 0x80, 0x00, 0x00 }; //128 bytes
  
  uint8_t lower_limit[] = { 0x00, 0x00, 0x00, 0x00};
  uint8_t upper_limit[] = { 0xE8, 0x03, 0x00, 0x00};
  uint8_t initial_value[] = { 0x0A, 0x00, 0x00, 0x00};
  uint8_t limited_credit = 0x00;
  
  // Wait for an ISO14443A card
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
  
  if (success) {
    Serial.println("Found an ISO14443A card");
    Serial.print("  UID Length: ");Serial.print(uidLength, DEC);Serial.println(" bytes");
    Serial.print("  UID Value: ");
    nfc.PrintHex(uid, uidLength);
    Serial.println("");
  
    //If uidLength is 7 it's possible this is a DESFire card
    if (uidLength == 7) {
      byte key[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
      uint8_t session_key[8];
      Serial.println("Trying to start desfire authentication");
      application[0] = 0xBE;
      application[1] = 0xEF;
      application[2] = 0x00;
      success = nfc.mifaredesfire_SelectApplication(application);
      
      if (success) {
          Serial.println("PICC Level selected, starting authentication");
          success = authenticate(key, session_key);
          
          if (success) {
            Serial.println("Authentication successful.");
             
             success = nfc.mifaredesfire_CreateStandardFile(std_filenum, comm_settings, access, filesize);
             success = nfc.mifaredesfire_CreateValueFile(val_filenum, comm_settings, access, lower_limit, upper_limit, initial_value, limited_credit);
          }
      }

    }
    else {
      Serial.println("UID length is not 7, this is not a DESFire card");
    }
  }
}
