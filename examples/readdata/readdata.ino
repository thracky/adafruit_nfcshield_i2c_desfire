#include <Adafruit_NFCShield_I2C.h>
#include <DES.h>
#include <Wire.h>

#define IRQ   (2)
#define RESET (3)  // Not connected by default on the NFC Shield

Adafruit_NFCShield_I2C nfc(IRQ, RESET);
DES des;


void setup() {
  Serial.begin(115200);
  Serial.println("Hello!");

  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }
  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  
  // configure board to read RFID tags
  nfc.SAMConfig();
  
  Serial.println("Waiting for an ISO14443A Card ...");
}



void loop() {
  uint8_t success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
  
  //Arrays for outgoing commands/incoming replies
  uint8_t data[128];
  uint8_t decrypted[128];
  uint8_t datalen = 0;
  uint8_t application[3];
  
  byte key[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  byte desout[8];
  byte desin[8];
  uint8_t tempblock[8];

  uint8_t std_filenum = 0x01;
  uint8_t val_filenum = 0x02;
  uint8_t offset[3];
  uint8_t numblocks = 13;

  // Wait for an ISO14443A card
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
  
  if (success) {
    Serial.println("Found an ISO14443A card");
    Serial.print("  UID Length: ");Serial.print(uidLength, DEC);Serial.println(" bytes");
    Serial.print("  UID Value: ");
    nfc.PrintHex(uid, uidLength);
    Serial.println("");
  
    //If uidLength is 7 it's possible this is a DESFire card
    if (uidLength == 7) {

      uint8_t session_key[8];
      Serial.println("Trying to start desfire authentication");
      application[0] = 0xBE;
      application[1] = 0xEF;
      application[2] = 0x00;
      success = nfc.mifaredesfire_SelectApplication(application);
      
      if (success) {
          Serial.println("PICC Level selected, starting authentication");
          success = authenticate(key, session_key);
          
          if (success) {
            Serial.println("Authentication successful.");
            offset[0] = 0x00;
            offset[1] = 0x00;
            offset[2] = 0x00;
            uint8_t bytes_to_read = 98;
            while (bytes_to_read > 0) {
            
             if(bytes_to_read >= 14) {
                success = nfc.mifaredesfire_ReadStandardFile(std_filenum, offset, 14, data, 16);
                Serial.println("Received encrypted data: ");
                nfc.PrintHex(data, 16);
                bytes_to_read -= 14;     
                int i;
                for (i = 0; i < 16; i++) {
                  tempblock[i%8] = data[i];
                  if (i%8 == 7) {
                    //If it's not the first block, need to XOR with last "plaintext" block.
                    if (i == 15) {
                      memcpy(desin, tempblock, 8);
                      nfc.PrintHex(desin, 8);
                      des.decrypt(desout, desin, session_key);
                      xor_bytes(desout, data, tempblock, 8);
                      nfc.PrintHexChar(tempblock, 8);
                      memcpy(decrypted+(offset[0]), tempblock, 6);
                      offset[0] += 6;
                    }
                    else {
                      memcpy(desin, tempblock, 8);
                      des.decrypt(desout, desin, session_key);
                      nfc.PrintHexChar(desout, 8);
                      memcpy(decrypted+(offset[0]), desout, 8);
                      offset[0] += 8;  
                  }
                    
                  }  
                }    
    
             }
             else { 
                 uint8_t pad_bytes;
                 if ((bytes_to_read+2) %8 != 0) {
                   pad_bytes = 8 - ((bytes_to_read+2) % 8);
                 }
                 success = nfc.mifaredesfire_ReadStandardFile(std_filenum, offset, bytes_to_read, data, 24); 
                 nfc.PrintHex(data, (bytes_to_read+2+pad_bytes));
                 int i;
                 for (i = 0; i < (bytes_to_read+2+pad_bytes); i++) {
                  desin[i%8] = data[i];
                  if (i%8 == 7) {
                        //If it's not the first block, need to XOR with last "plaintext" block.
                    if (i > 7) {
                      xor_bytes(desin, data+(i-15), desin, 8);
                      des.decrypt(desout, desin, session_key);
                    }
                    else {
                      des.decrypt(desout, desin, session_key);
                    }
                    if (i == (bytes_to_read+pad_bytes+1)) {
                      if (((i%8)-pad_bytes-2) > 0) {
                        memcpy(decrypted+(offset[0]), desout, ((i%8)-pad_bytes-2));
                        offset[0] += ((i%8)-pad_bytes-2);
                      }
                    }
                    else {
                      memcpy(decrypted+(offset[0]), desout, 8);
                      offset[0] += 8;
                    }
                  }  
                }
                bytes_to_read = 0;
             }
      }
           Serial.println("Data reading complete.");
           nfc.PrintHexChar(decrypted, 98);
      }

    }
    else {
      Serial.println("UID length is not 7, this is not a DESFire card");
    }
  }
}
}
