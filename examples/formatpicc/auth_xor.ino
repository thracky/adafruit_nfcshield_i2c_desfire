void xor_bytes(uint8_t* array1, uint8_t* array2, uint8_t* dest, uint8_t len) {
  uint8_t i;  
  for (i = 0; i < len; i++){
      dest[i] = array1[i]^array2[i];
  }
}

uint8_t authenticate(byte* key, uint8_t* session_key) {
  uint8_t success;
  
  //Arrays for outgoing commands/incoming replies
  uint8_t data[16];
  uint8_t datalen = 0;
  
  //Byte arrays for DES encryption
  byte desout[8];
  byte desin[8];
  
  //RndA/B and rotated values.
  uint8_t RndA[8];
  uint8_t RndB[8];
  uint8_t RndARot[8];
  uint8_t RndBRot[8];

  //Utility stuff for generating/encrypting RndA
  byte encRndA[8];
  long RndAleft;
  long RndAright;
      
  success = nfc.mifaredesfire_AuthStart(data, &datalen);
      
  if (success) {
  
    memcpy(desin, data, 8);
    des.decrypt(desout, desin, key);
  
    Serial.print("RndB decrypted bytes: ");
    nfc.PrintHex(desout, 8);
    memcpy(RndB, desout, 8);
   
    Serial.println("Rotating RndB");
    int i;
    for (i = 0; i < 8; i++) {
      if (i < 7)
        RndBRot[i] = desout[i+1];
      else
        RndBRot[i] = desout[0];
    }
    nfc.PrintHex(RndBRot, 8);
    
    Serial.println("Generating RndA");
    randomSeed(analogRead(0));
    RndAleft = random(2147438647);
    RndAright = random(2147438647);
    memcpy(RndA, &RndAleft, 4);
    memcpy(RndA+4, &RndAright, 4);
    nfc.PrintHex(RndA, 8);      
    
    Serial.println("*de*crypting RndA");
    memcpy(desin, RndA, 8);
    des.decrypt(desout, desin, key);
    
    memcpy(data, desout, 8);
    memcpy(encRndA, desout, 8);
    
    xor_bytes(encRndA, RndBRot, desin, 8);
        
    nfc.PrintHex((uint8_t*)desin, 8);    
    
    Serial.println("*de*crypting RndBRot in CBC send mode");
    des.decrypt(desout, desin, key);
    memcpy(data+8, desout, 8);
    nfc.PrintHex(data, 16);
    datalen = 16;
       
    success = nfc.mifaredesfire_AuthStep2(data, &datalen);
    if (success) {
      nfc.PrintHex(data, 8);
      memcpy(desin, data, 8);
      des.decrypt(desout, desin, key);
      for (i = 0; i < 8; i++) {
        if (i < 7)
          RndARot[i] = RndA[i+1];
        else
          RndARot[i] = RndA[0];
      }
      success = 1;
      for (i = 0; i < 8; i++) {
        if (RndARot[i] != desout[i]) {
          success = 0;
        }
      }
      if (success) {
        Serial.println("Building session key");
        memcpy(session_key, RndA, 4);
        memcpy(session_key+4, RndB, 4);
        nfc.PrintHex(session_key, 8);
        return 1;
      } // end if our RndARot's match
    } // end if auth step 2 was successful
  } // end if AuthStart was successful
}
