#include <Adafruit_NFCShield_I2C.h>
#include <DES.h>
#include <Wire.h>

#define IRQ   (2)
#define RESET (3)  // Not connected by default on the NFC Shield

Adafruit_NFCShield_I2C nfc(IRQ, RESET);
DES des;


void setup() {
  Serial.begin(115200);
  Serial.println("Hello!");

  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }
  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata >> 24) & 0xFF, HEX);
  Serial.print("Firmware ver. "); Serial.print((versiondata >> 16) & 0xFF, DEC);
  Serial.print('.'); Serial.println((versiondata >> 8) & 0xFF, DEC);

  // configure board to read RFID tags
  nfc.SAMConfig();

  Serial.println("Waiting for an ISO14443A Card ...");
}

void loop() {
  uint8_t success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

  //Arrays for outgoing commands/incoming replies
  uint8_t data[128];
  uint8_t datalen = 0;
  uint8_t application[3];

  byte key[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  byte desout[8];
  byte desin[8];

  uint8_t creditamount[] = { 0x64, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  uint8_t debitamount[] = { 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  nfc.iso14443a_crc_append(creditamount, 4);
  nfc.iso14443a_crc_append(debitamount, 4);

  uint8_t val_filenum = 0x02;

  // Wait for an ISO14443A card
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);

  if (success) {
    Serial.println("Found an ISO14443A card");
    nfc.PrintHex(uid, uidLength);

    //If uidLength is 7 it's possible this is a DESFire card
    if (uidLength == 7) {

      uint8_t session_key[8];
      application[0] = 0xBE;
      application[1] = 0xEF;
      application[2] = 0x00;
      success = nfc.mifaredesfire_SelectApplication(application);

      if (success) {
        success = authenticate(key, session_key);

        if (success) {
          Serial.println("Authentication successful.");

                
                Serial.println("Performing credit - 100");
                memcpy(desin, creditamount, 8);
                des.decrypt(desout, desin, session_key);
                
                success = nfc.mifaredesfire_Credit(val_filenum, desout);
                if (success) {
                  success = nfc.mifaredesfire_CommitTransaction();
                  if (success) {
                    Serial.println("Credit of 100 applied successfully. Checking balance.");
                    success = nfc.mifaredesfire_GetValue(val_filenum, data);
                    if (success) {
                      memcpy(desin, data, 8);
                      des.decrypt(desout, desin, session_key);
                      nfc.PrintHex(desout,8);
                    }
                  }
                }
                
          Serial.println("Performing debit - 12");
          memcpy(desin, debitamount, 8);
          des.decrypt(desout, desin, session_key);
                
          success = nfc.mifaredesfire_Debit(val_filenum, desout);
          if (success) {
            success = nfc.mifaredesfire_CommitTransaction();
            if (success) {
              Serial.println("Debit of 12 applied successfully. Checking balance.");
              success = nfc.mifaredesfire_GetValue(val_filenum, data);
              if (success) {
                memcpy(desin, data, 8);
                des.decrypt(desout, desin, session_key);
                nfc.PrintHex(desout, 8);
              }
            }
          }
        }
      }
    }
  }
}
